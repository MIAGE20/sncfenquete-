package com.example.sncfenquete;

import android.util.Log;

import java.util.HashMap;

public class Enquete {
    private HashMap < String, Candidat> lesCandidats ;
    public Enquete()
    {
        this.lesCandidats = new HashMap<String, Candidat>();
    }

    public void ajouterCandidat(Candidat unCandidat)
    {
        this.lesCandidats.put (unCandidat.getPseudo(), unCandidat);
       // Log.e("ajout", unCandidat.getPseudo());
    }

    public void ajouterReponse (String pseudo, String question, int score)
    {
        this.lesCandidats.get(pseudo).ajouterReponse(question, score);
    }

    public float getMoyenne (String pseudo)
    {
        return this.lesCandidats.get(pseudo).calculMoyenne();
    }

    public int getSmiley (String pseudo)
    {
        return this.lesCandidats.get(pseudo).getSmiley();
    }

    public Candidat getCandidat (String pseudo)
    {
        return this.lesCandidats.get(pseudo);
    }

    public HashMap<String, Candidat> getLesCandidats() {
        return lesCandidats;
    }
}
