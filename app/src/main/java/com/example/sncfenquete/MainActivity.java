package com.example.sncfenquete;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{

    private ImageButton ibRerA, ibRerB, ibRerC, ibRerD, ibRerE;
    private static int fait = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.ibRerA = (ImageButton) findViewById(R.id.idRerA);
        this.ibRerB = (ImageButton) findViewById(R.id.idRerB);
        this.ibRerC = (ImageButton) findViewById(R.id.idRerC);
        this.ibRerD = (ImageButton) findViewById(R.id.idRerD);
        this.ibRerE = (ImageButton) findViewById(R.id.idRerE);

        //rendre les images cliquables
        this.ibRerA.setOnClickListener(this);
        this.ibRerB.setOnClickListener(this);
        this.ibRerC.setOnClickListener(this);
        this.ibRerD.setOnClickListener(this);
        this.ibRerE.setOnClickListener(this);
        //initialisation des enquetes
        if (fait ==0){
            SNCF.initialiser();
            fait = 1;
        }

    }

    @Override
    public void onClick(View v) {
        String rer= "";
        switch (v.getId())
        {
            case R.id.idRerA : rer = "RERA"; break;
            case R.id.idRerB : rer = "RERB"; break;
            case R.id.idRerC : rer = "RERC"; break;
            case R.id.idRerD : rer = "RERD"; break;
            case R.id.idRerE : rer = "RERE"; break;
        }
        Intent unIntent = new Intent(this, Inscription.class);
        unIntent.putExtra("rer",rer);
        this.startActivity(unIntent);
    }
}
