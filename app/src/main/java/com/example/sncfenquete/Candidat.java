package com.example.sncfenquete;

import java.util.HashMap;

public class Candidat
{
    private String nom, prenom, pseudo, frequence, tranche;

    private HashMap<String, Integer> lesReponses;

    public Candidat(String nom, String prenom, String pseudo, String frequence, String tranche) {
        this.nom = nom;
        this.prenom = prenom;
        this.pseudo = pseudo;
        this.frequence = frequence;
        this.tranche = tranche;
        this.lesReponses = new HashMap<String, Integer>();
    }

    public String getFrequence() {
        return frequence;
    }

    public void setFrequence(String frequence) {
        this.frequence = frequence;
    }

    public String getTranche() {
        return tranche;
    }

    public void setTranche(String tranche) {
        this.tranche = tranche;
    }

    public void ajouterReponse (String question, int score)
    {
        this.lesReponses.put(question, score);
    }

    public float calculMoyenne()
    {
        float moy = 0;
        for (Integer score : this.lesReponses.values())
        {
            moy += score;
        }
        moy /= this.lesReponses.size();
        return moy;
    }
    public int getSmiley()
    {
        float moy = calculMoyenne();
        if (moy < 10 ) return R.drawable.smiley3;
        else if (moy <14 ) return R.drawable.smiley2;
        else return R.drawable.smiley1;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public HashMap<String, Integer> getLesReponses() {
        return lesReponses;
    }

    public void setLesReponses(HashMap<String, Integer> lesReponses) {
        this.lesReponses = lesReponses;
    }
}
