package com.example.sncfenquete;

import java.util.HashMap;

public class SNCF
{
    private static HashMap<String, Enquete> lesEnquetes = new HashMap<String, Enquete>();

    public static void initialiser ()
    {
        lesEnquetes.put("RERA", new Enquete() );
        lesEnquetes.put("RERB", new Enquete() );
        lesEnquetes.put("RERC", new Enquete() );
        lesEnquetes.put("RERD", new Enquete() );
        lesEnquetes.put("RERE", new Enquete() );
    }

    public static Enquete getEnquete (String rer)
    {
        return lesEnquetes.get(rer);
    }
}
