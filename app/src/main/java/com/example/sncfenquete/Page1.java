package com.example.sncfenquete;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Arrays;

public class Page1 extends AppCompatActivity implements View.OnClickListener {

    private TextView tvTitre ;
    private RadioGroup rgFrequence, rgPonctualite;
    private Button btSuivant ;
    private String rer, pseudo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page1);
        this.tvTitre = (TextView)findViewById( R.id.idTitre);
        this.rgFrequence = (RadioGroup) findViewById(R.id.idGroupe1);
        this.rgPonctualite = (RadioGroup) findViewById(R.id.idGroupe2);

        this.btSuivant=(Button) findViewById(R.id.idSuivant);

        this.rer = this.getIntent().getStringExtra("rer").toString();
        String tab[] = this.getIntent().getStringExtra("tab").toString().split(" - ");
        System.out.println(Arrays.toString(tab));
        this.pseudo = tab[0];
        this.tvTitre.setText("Bienvenue Mm/M "+tab[1] + " "+ tab[2]);

        //rendre le bouton cliquable
        this.btSuivant.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.idSuivant)
        {
            //traitement des réponses
            String question = "frequence";
            int score = 0 ;
            switch (this.rgFrequence.getCheckedRadioButtonId())
            {
                case R.id.idFrequence1 : score = 8; break;
                case R.id.idFrequence2 : score = 12; break;
                case R.id.idFrequence3 : score = 16; break;
            }
            SNCF.getEnquete(this.rer).ajouterReponse(this.pseudo, question, score);

            question = "ponctualite";
            score = 0;
            switch (this.rgPonctualite.getCheckedRadioButtonId())
            {
                case R.id.idPonctualite1 : score = 8; break;
                case R.id.idPonctualite2 : score = 12; break;
                case R.id.idPonctualite3 : score = 16; break;
            }
            SNCF.getEnquete(this.rer).ajouterReponse(this.pseudo, question, score);

            Intent unIntent = new Intent(this, Page2.class);
            unIntent.putExtra("rer", this.rer);
            unIntent.putExtra("pseudo", this.pseudo);
            this.startActivity(unIntent);
        }
    }
}
