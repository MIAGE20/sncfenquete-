package com.example.sncfenquete;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class Inscription extends AppCompatActivity implements View.OnClickListener {

    private String rer ;
    private Spinner spFrequence, spTranche;
    private EditText txtNom, txtPrenom, txtPseudo;
    private Button btParticiper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        this.rer = this.getIntent().getStringExtra("rer");
        Toast.makeText(this, "Bienvenue Enquete" + this.rer, Toast.LENGTH_SHORT).show();

        this.spFrequence = (Spinner) findViewById(R.id.idFrequence);
        this.spTranche = (Spinner) findViewById(R.id.idTranche);

        this.txtNom = (EditText) findViewById(R.id.idNom);
        this.txtPrenom = (EditText) findViewById(R.id.idPrenom);
        this.txtPseudo = (EditText) findViewById(R.id.idPseudo);

        this.btParticiper = (Button) findViewById(R.id.idParticiper);
        //remplir les spinners
        ArrayList<String> frequences = new ArrayList<String>();
        frequences.add("Quotidienne");
        frequences.add("Hebdomadaire");
        frequences.add("Mensuelle");
        frequences.add("Annuelle");
        frequences.add("Autre");

        //adaptateur d'affichage
        ArrayAdapter unAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, frequences);
        this.spFrequence.setAdapter(unAdapter);



        ArrayList<String> tranches = new ArrayList<String>();
        tranches.add("0 - 12 ans");
        tranches.add("13 - 18 ans");
        tranches.add("19 - 35 ans");
        tranches.add("36 - 60 ans");
        tranches.add("+ de 60 ans");

        //adaptateur d'affichage
        ArrayAdapter unAdapter2 = new ArrayAdapter(this, android.R.layout.simple_spinner_item, tranches);
        this.spTranche.setAdapter(unAdapter2);

        this.btParticiper.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.idParticiper)
        {
            String nom = this.txtNom.getText().toString();
            String prenom = this.txtPrenom.getText().toString();
            String pseudo = this.txtPseudo.getText().toString();

            String frequence = this.spFrequence.getSelectedItem().toString();
            String tranche = this.spTranche.getSelectedItem().toString();
            //inscription du candidat
            Candidat unCandidat = new Candidat(nom, prenom, pseudo, frequence, tranche);
            SNCF.getEnquete(this.rer).ajouterCandidat(unCandidat);

            //envoi vers la page enquete 1
            Intent unIntent = new Intent(this, Page1.class);
            unIntent.putExtra("rer", this.rer);
            unIntent.putExtra("tab", pseudo+ " - " + nom + " - " + prenom);
            this.startActivity(unIntent);


        }

    }
}
