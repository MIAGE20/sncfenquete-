package com.example.sncfenquete;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

public class Page2 extends AppCompatActivity implements View.OnClickListener {

    private RadioGroup rgInformation, rgProprete;
    private Button btSuivant ;
    private String rer, pseudo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page2);

        this.rgInformation = (RadioGroup) findViewById(R.id.idGroupe3);
        this.rgProprete = (RadioGroup) findViewById(R.id.idGroupe4);
        this.btSuivant=(Button) findViewById(R.id.idSuivant2);

        this.rer = this.getIntent().getStringExtra("rer").toString();
        this.pseudo = this.getIntent().getStringExtra("pseudo").toString();

        this.btSuivant.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.idSuivant2)
        {
            //traitement des réponses
            String question = "information";
            int score = 0 ;
            switch (this.rgInformation.getCheckedRadioButtonId())
            {
                case R.id.idInformation1 : score = 8; break;
                case R.id.idInformation2 : score = 12; break;
                case R.id.idInformation3 : score = 16; break;
            }
            SNCF.getEnquete(this.rer).ajouterReponse(this.pseudo, question, score);

            question = "proprete";
            score = 0;
            switch (this.rgProprete.getCheckedRadioButtonId())
            {
                case R.id.idProprete1 : score = 8; break;
                case R.id.idProprete2 : score = 12; break;
                case R.id.idProprete3 : score = 16; break;
            }
            SNCF.getEnquete(this.rer).ajouterReponse(this.pseudo, question, score);

            Intent unIntent = new Intent(this, Fin.class);
            unIntent.putExtra("rer", this.rer);
            unIntent.putExtra("pseudo", this.pseudo);
            this.startActivity(unIntent);

        }
    }
}
