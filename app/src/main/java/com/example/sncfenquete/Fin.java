package com.example.sncfenquete;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Fin extends AppCompatActivity implements View.OnClickListener {

    private TextView txtResultat ;
    private String rer, pseudo ;
    private ImageView ivSmiley;

    private Button mp;
    private ListView lvResultats ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fin);

        this.txtResultat = (TextView)findViewById(R.id.idResultat);
        this.ivSmiley = (ImageView) findViewById(R.id.idSmiley);

        this.mp = (Button) findViewById( R.id.idMp);
        this.lvResultats = (ListView) findViewById(R.id.idListe) ;

        this.rer = this.getIntent().getStringExtra("rer").toString();
        this.pseudo = this.getIntent().getStringExtra("pseudo").toString();

        Candidat unCandidat = SNCF.getEnquete(this.rer).getCandidat(this.pseudo);
        float moy = SNCF.getEnquete(this.rer).getMoyenne(this.pseudo);
        this.txtResultat.setText(unCandidat.getNom()+ " "+ unCandidat.getPrenom()+ " NOTE :"+ moy);

        this.ivSmiley.setImageResource(SNCF.getEnquete(this.rer).getSmiley(this.pseudo));

        //construction de la liste view et un bouton retour de la mainactivity
        this.mp.setOnClickListener(this);

        ArrayList <String> lesResultats = new ArrayList<String>();
        for (Candidat unC : SNCF.getEnquete(this.rer).getLesCandidats().values())
        {
            lesResultats.add(unC.getNom()+ " " + unC.getPrenom() + " Note : " + unC.calculMoyenne() );
            Log.e("nom", unC.getNom()+ "  "+ this.rer);
        }
        ArrayAdapter unAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, lesResultats);
        this.lvResultats.setAdapter(unAdapter);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.idMp)
        {
            Intent unIntent = new Intent(this,MainActivity.class);
            this.startActivity(unIntent);
        }

    }
}
